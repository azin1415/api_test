from modules.scraper import ApiScraper
from modules.response.responses.paypal_response import PayPalResponse
from modules.db.dynamo_controller import DynamoDbController

scraper = ApiScraper(response=PayPalResponse(), api="https://www.paypal-status.com/api/v1/events")
response_data = scraper.scrap_api()
scraper.save_api_response('data')
db_controller = DynamoDbController()
db_controller.get_instance().print_data(response_data, response=PayPalResponse())
