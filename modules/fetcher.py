import requests


class Connection:

    def __init__(self, api):
        self.api = api
        self.response = requests.get(api)
        if self.response.status_code != 200:
            print("Status code is not ok! current status code " + self.response.status_code)
            exit(1)
        else:
            print("initial Status code is 200")

    def get_response_content(self):
        return self.response.json()

    def fetch(self):
        self.response = requests.get(self.api)
        if self.response.status_code != 200:
            print("Status code is not ok! current status code " + self.response.status_code)
            exit(1)
        else:
            print("Fetching: Status code is 200")
            return self.get_response_content()