import json
import boto3
from botocore.config import Config
from modules.response.responses.paypal_response import PayPalResponse


class DynamoDbController:
    instance = None
    table = ...
    @staticmethod
    def get_instance():
        if DynamoDbController.instance is None:
            DynamoDbController.instance = DynamoDbController()
        return DynamoDbController.instance

    def __init__(self):
        if DynamoDbController.instance is None:
            my_config = Config(
                region_name='eu-west-3',
                signature_version='v4',
                retries={
                    'max_attempts': 10,
                    'mode': 'standard'
                }
            )
            self.dynamodb = boto3.resource('dynamodb', config=my_config, aws_access_key_id="AKIAQSPNLRJY5LDJFQGX",
                                            aws_secret_access_key="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")
            self.table = self.dynamodb.Table('InterviewTable-53e7594f-f10e')
            print("table is: InterviewTable-53e7594f-f10e")

        else:
            DynamoDbController.__instance = self

    def send_data(self, content, response):
        data_json = json.loads(content)
        if type(response) is PayPalResponse:
            for result in data_json['events']:
                id = result['id']
                print("Adding id:", id)
                print(result)
                DynamoDbController.instance.table.put_item(Item=result)
        else:
            print("no response was found")

    def print_data(self, content, response):
        data_json = json.loads(content)
        if type(response) is PayPalResponse:
            for result in data_json['events']:
                db_response = DynamoDbController.instance.table.get_item(Key={'id': result['id']})
                print(db_response)
        else:
            print("no response was found")
