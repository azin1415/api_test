import re


class JsonUtils:

    @staticmethod
    def remove_tags(text):
        regex = re.compile('<.*?>')
        clean_text = re.sub(regex, '', text)
        return clean_text
