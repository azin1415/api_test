from modules.fetcher import Connection
import json

class ApiScraper:
    def __init__(self, response, api):
        self.response = response
        self.api = api
        self.connection = Connection(api)
        self.response = response
        self.response_content = ...

    def scrap_api(self):
        self.response_content = self.response.build_json(self.connection.fetch())
        return self.response_content

    def save_api_response(self, name):
        with open(name+'.json', 'w') as f:
            f.write(self.response_content)

