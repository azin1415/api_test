import json
from modules.utils.json_utils import JsonUtils


class PayPalResponse:

    def __init__(self):
        self.response = {}

    def build_json(self, content):

        data = {'events': []}
        try:
            for item in content['result']:
                event = {
                    'id': str(item['id']),
                    'body_message': JsonUtils.remove_tags(item['body']),
                    'environment': item['environment'],
                    'type': item['type'],
                    'pubDate': item['pubDate'],
                    'startDate': item['startDate'],
                    'endDate': item['endDate'],
                    'state': item['state'],
                    'impactedComponents': [],
                }
                for comp in item['impactedComponents']:
                    component = {
                        "id": comp['id'],
                        'name': comp['name'],
                        'category': {'name': comp['category']['name'],
                                     'description': comp['category']['description']},
                        'status': {'production': comp['status']['production'],
                                   'sandbox': comp['status']['sandbox']}
                    }
                    event['impactedComponents'].append(component)

                data['events'].append(event)
            self.response = json.dumps(data, indent=4, sort_keys=True)
            return self.response
        except KeyError as e:
            print("PayPal API JSON failed to parse")
            print("key failed to parse: "+str(e))
            return {}


